import { resolve } from 'path';
import { Configuration } from 'webpack';
const CleanWebpackPlugin = require('clean-webpack-plugin');

function config(env: object, { mode }: { mode: string }): Configuration {
	const returnable: Configuration = {
		devtool: mode === 'production' ? false : 'cheap-module-eval-source-map',
		entry: ['babel-polyfill', resolve(__dirname, 'lib/index.js')],
		module: {
			rules: [
				// { test: /\.ts$/, exclude: /node_modules/, use: ['babel-loader', 'ts-loader'] },
				{ test: /\.jsx?$/, exclude: /node_modules/, loader: 'babel-loader' },
			],
		},
		optimization: {
			splitChunks: {
				chunks: 'all',
			},
		},
		output: {
			filename: 'index.js',
			library: 'lingotekRestClient',
			libraryTarget: 'umd',
			path: resolve(__dirname, 'dist'),
		},
		plugins: [new CleanWebpackPlugin(['dist'])],
		resolve: {
			extensions: ['.ts', '.js'],
		},
	};
	return returnable;
}

export default config;
