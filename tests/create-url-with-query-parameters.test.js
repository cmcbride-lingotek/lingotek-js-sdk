import { createUrlWithQueryParameters } from '../lib/create-url-with-query-parameters';
const url = 'https://www.lingotek.com';
const queryParams = {
	a: 'b',
	c: ['d', 'e'],
	f: {
		g: 'h',
	},
};

test('should be able to create a url with query params, without dividing arrays', () => {
	const expected = 'https://www.lingotek.com?a=b&c=d%2Ce&f.g=g';
	expect(createUrlWithQueryParameters({ url, queryParams })).toEqual(expected);
});

test('should be able to create a url with query params, dividing arrays', () => {
	const expected = 'https://www.lingotek.com?a=b&c=d&c=e&f.g=g';
	expect(createUrlWithQueryParameters({ url, queryParams, divideArrays: true })).toEqual(expected);
});
