import { getResponseBody } from '../lib/get-response-body';
global.Headers = Map;

let expected, response;
beforeEach(() => {
	expected = 'json';
	response = {
		headers: new Headers(),
		json() {
			return expected;
		},
		formData() {
			return expected;
		},
		text() {
			return expected;
		},
		blob() {
			return expected;
		},
	};
	response.headers.set('content-type', 'application/json');
});

test('should be able to get the response body based on the header - json', () => {
	expect(getResponseBody(response)).toBe(expected);
});

test('should be able to get the response body based on the header - formData', () => {
	response.headers = new Headers();
	response.headers.set('Content-Type', 'application/x-www-form-urlencoded');
	expect(getResponseBody(response)).toBe(expected);
});

test('should be able to get the response body based on the header - text', () => {
	response.headers = new Headers();
	response.headers.set('Content-Type', 'text/plain');
	expect(getResponseBody(response)).toBe(expected);
});
