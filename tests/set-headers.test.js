import { setHeaders } from '../lib/set-headers';
global.Headers = Map;

let expected, fetchParams, authToken;

beforeEach(() => {
	authToken = 'great-auth-token';
	fetchParams = {};
	expected = new Headers();
	expected.set('Authorization', `Bearer ${authToken}`);
	expected.set('Content-Type', 'application/json');
});

test('should set the correct header without existing headers', () => {
	expect(setHeaders({ fetchParams, authToken })).toEqual(expected);
});

test('should set the correct header with existing headers', () => {
	fetchParams.headers = new Headers();
	fetchParams.headers.set('Response-Type', 'application/json');
	expected.set('Response-Type', 'application/json');
	expect(setHeaders({ fetchParams, authToken })).toEqual(expected);
});

test('should fail when forgetting an auth token', () => {
	expect(() => setHeaders({ fetchParams })).toThrowError();
});
