import { methodIsValid } from '../lib/method-is-valid';

test('should accept valid methods', () => {
	const valid = ['GET', 'POST', 'PATCH', 'DELETE'];
	for (const method of valid) {
		expect(methodIsValid(method)).toBe(true);
	}
});

test('should reject invalid methods', () => {
	const invalid = ['DRINK_TEA', 'PUT'];
	for (const method of invalid) {
		expect(methodIsValid(method)).toBe(false);
	}
});
