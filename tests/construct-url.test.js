import { constructUrl } from '../lib/construct-url';

test('should be able to put together a base url and a path', () => {
	const baseUrl = 'https://google.com';
	const path = '/path';
	const expected = 'https://google.com/path';
	expect(constructUrl(baseUrl, path)).toEqual(expected);
	// with slash at the end
	expect(constructUrl(`${baseUrl}/`, path)).toEqual(expected);
	// with no slash at the front of path
	expect(constructUrl(baseUrl, path.substr(1))).toEqual(expected);
});
