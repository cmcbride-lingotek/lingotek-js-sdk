Lingotek Javascript SDK
===
[API]: http://devzone.lingotek.com
[website]: http://devzone.lingotek.com

The [Lingotek Platform][website] is
a set of APIs that allow you to push and pull multilingual
content from the translation hub.

This repository contains an open source Lingotek SDK that allows you to
access the Lingotek platform using Javascript. 

Contributing
------------
This open source project was started by Lingotek developers, but is now
open to developers world-wide.

Report Issues/Bugs
------------------
- [Bugs](http://devzone.lingotek.com/contact)
- [Questions](http://devzone.lingotek.com/contact)

---
Under [BSD 3-Clause License](/license.md)