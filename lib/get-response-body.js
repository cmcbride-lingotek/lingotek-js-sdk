export function getResponseBody(response) {
	const contentType = response.headers.get('Content-Type') || response.headers.get('content-type');
	if (contentType.includes('json')) {
		return response.json();
	} else if (contentType.includes('form')) {
		return response.formData();
	} else if (contentType.includes('text')) {
		return response.text();
	}
	return contentType.blob();
}
