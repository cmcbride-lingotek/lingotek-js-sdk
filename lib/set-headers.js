export function setHeaders({ fetchParams, authToken }) {
	let headers = new Headers();
	if (fetchParams.headers instanceof Headers) {
		headers = fetchParams.headers;
	} else if (fetchParams.headers instanceof Object) {
		headers = new Headers(fetchParams.headers);
	}
	// add the authorization header
	if (!authToken) {
		throw new Error('Authorization token is required');
	}
	headers.set('Authorization', `Bearer ${authToken}`);
	// set the expected response type
	if (!headers.has('Content-Type')) {
		headers.set('Content-Type', 'application/json');
	}
	return headers;
}
