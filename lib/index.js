import 'babel-polyfill';
import 'isomorphic-fetch';
import { request } from './request';
import { constructUrl } from './construct-url';

export default function client(baseUrl, authToken) {
	return {
		getRequest: ({
			path,
			fetchParams,
			requestBody = {},
			useUrlQueryParams = false,
			divideArrays = false,
		}) => {
			const method = 'GET';
			const fullUrl = constructUrl(baseUrl, path);
			return request({
				fullUrl,
				authToken,
				fetchParams,
				requestBody,
				method,
				useUrlQueryParams,
				divideArrays,
			});
		},
		postRequest: ({
			path,
			fetchParams,
			requestBody = {},
			useUrlQueryParams = false,
			divideArrays = false,
		}) => {
			const method = 'POST';
			const fullUrl = constructUrl(baseUrl, path);
			return request({
				fullUrl,
				authToken,
				fetchParams,
				requestBody,
				method,
				useUrlQueryParams,
				divideArrays,
			});
		},
		patchRequest: ({
			path,
			fetchParams,
			requestBody = {},
			useUrlQueryParams = false,
			divideArrays = false,
		}) => {
			const method = 'PATCH';
			const fullUrl = constructUrl(baseUrl, path);
			return request({
				fullUrl,
				authToken,
				fetchParams,
				requestBody,
				method,
				useUrlQueryParams,
				divideArrays,
			});
		},
		deleteRequest: ({
			path,
			fetchParams,
			requestBody = {},
			useUrlQueryParams = false,
			divideArrays = false,
		}) => {
			const method = 'DELETE';
			const fullUrl = constructUrl(baseUrl, path);
			return request({
				fullUrl,
				authToken,
				fetchParams,
				requestBody,
				method,
				useUrlQueryParams,
				divideArrays,
			});
		},
	};
}
