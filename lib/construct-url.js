export function constructUrl(baseUrl, path) {
	let url = baseUrl;
	// if baseUrl does not end with a slash;
	if (!/\/$/.test(baseUrl)) {
		// add a trailing / to baseUrl;
		url += '/';
	}
	// if path starts with a slash
	if (/^\//.test(path)) {
		// remove it
		url += path.substring(1);
	} else {
		url += path;
	}
	return url;
}
