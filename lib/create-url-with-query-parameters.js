export function createUrlParameter({ key, value, divideArrays = false }) {
	if (Array.isArray(value)) {
		const params = [];
		if (divideArrays) {
			for (const arrayValue of value) {
				params.push(`${encodeURIComponent(key)}=${encodeURIComponent(arrayValue)}`);
			}
			return params.join('&');
		} else {
			return `${encodeURIComponent(key)}=${encodeURIComponent(value)}`;
		}
	} else if (value instanceof Object) {
		const params = [];
		for (const [subKey, subValue] of Object.entries(value)) {
			params.push(createUrlParameter({ key: `${key}.${subKey}`, value: subKey, divideArrays }));
		}
		return params.join('&');
	} else {
		return `${encodeURIComponent(key)}=${encodeURIComponent(value)}`;
	}
}

export function createUrlWithQueryParameters({ url, queryParams, divideArrays = false }) {
	let finalUrl = url;
	const queryParamsArray = Object.entries(queryParams);
	if (queryParamsArray.length === 0) {
		return finalUrl;
	}
	finalUrl += '?';
	const params = [];
	for (const [key, value] of queryParamsArray) {
		params.push(createUrlParameter({ key, value, divideArrays }));
	}
	finalUrl += params.join('&');
	return finalUrl;
}
