export const allowedMethods = Object.freeze(['GET', 'POST', 'PATCH', 'DELETE']);

export function methodIsValid(method) {
	return allowedMethods.includes(method);
}
