import { createUrlWithQueryParameters } from './create-url-with-query-parameters';
import { methodIsValid, allowedMethods } from './method-is-valid';
import { setHeaders } from './set-headers';
import { getResponseBody } from './get-response-body';

export async function request({
	fullUrl,
	authToken,
	fetchParams = {},
	requestBody = {},
	method = 'GET',
	useUrlQueryParams = false,
	divideArrays = false,
}) {
	const params = { ...fetchParams, method, body: requestBody };
	if (!methodIsValid(method)) {
		throw new Error(
			`Method '${method}' is invalid. Must be one of the following: ${allowedMethods.join(', ')}.`
		);
	}
	// always use query params for GET
	let url = fullUrl;
	if (useUrlQueryParams || method === 'GET') {
		url = createUrlWithQueryParameters({ url, divideArrays, queryParams: params.body });
	}
	params.headers = setHeaders({ fetchParams: params, authToken });

	try {
		const fetchResponse = await fetch(url, params);
	} catch (err) {
		throw new Error(`Request failed.\n${fetchResponse.message}`);
	}
	if (!fetchResponse.ok) {
		throw new Error(`Request failed.\n${fetchResponse.status} - ${fetchResponse.statusText}`);
	}
	const responseBody = getResponseBody(fetchResponse);
	if (responseBody.message !== undefined) {
		throw new Error('Request failed.\n' + responseBody.message.join('\n'));
	}
	return responseBody;
}
